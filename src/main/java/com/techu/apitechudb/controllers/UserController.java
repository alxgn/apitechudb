package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(name = "$orderBy", required = false) String orderBy){
        System.out.println("getUsers");
        List<UserModel> resultados;
        if(orderBy!=null && orderBy.equalsIgnoreCase("age")){
            System.out.println("Hemos recibido la solicitud de ordenación: "+orderBy);
            resultados = userService.findAllOrderBy(orderBy);
        }else{
            resultados = userService.findAll();
        }

        return new ResponseEntity<>(
                resultados,
                HttpStatus.OK
        );
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById");
        System.out.println("Id del usuario a buscar "+id);

        Optional<UserModel> result = userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser");
        System.out.println("Id del usuario a crear "+user.getId());
        System.out.println("Nombre del usuario a crear "+user.getName());
        System.out.println("Edad del usuario a crear "+user.getAge());

        return new ResponseEntity<>(
                this.userService.add(user),
                HttpStatus.CREATED
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<Object> updateUser(@RequestBody UserModel user, @PathVariable String id) {
        System.out.println("updateUser");
        System.out.println("Id del usuario a actualizar por url " + id);
        System.out.println("Id del usuario a actualizar " + user.getId());
        System.out.println("Nombre del usuario a actualizar " + user.getName());
        System.out.println("Edad del usuario a actualizar " + user.getAge());
        if(!id.equalsIgnoreCase(user.getId())){
            return new ResponseEntity<>(
                    "La id del usuario no es modificable",
                    HttpStatus.BAD_REQUEST
            );
        }
        Optional<UserModel> userUpdate = userService.findById(id);
        if(userUpdate.isPresent()){
            System.out.println("Usuario para actualizar encontrado. Se actualiza");
            userService.update(user);
        }

        return new ResponseEntity<>(
                user,
                userUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){
        System.out.println("deleteUser");
        System.out.println("Id del usuario a eliminar " + id);
        boolean deleteProduct = userService.delete(id);
        return new ResponseEntity<>(
                deleteProduct ? "Usuario borrado" : "Usuario no borrado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
