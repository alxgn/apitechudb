package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts(){
        System.out.println("getProducts");

        //return this.productService.findAll();
        return new ResponseEntity<>(
                this.productService.findAll(),
                HttpStatus.OK
        );
    }
    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("Id del producto a buscar "+id);

        Optional<ProductModel> result = productService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){
        System.out.println("addProduct");
        System.out.println("Id del producto a crear "+product.getId());
        System.out.println("Descripcion del producto a crear "+product.getDesc());
        System.out.println("Precio del producto a crear "+product.getPrice());

        return new ResponseEntity<>(
                this.productService.add(product),
                HttpStatus.CREATED
        );
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("Id del producto a actualizar por url " + id);
        System.out.println("Id del producto a actualizar " + product.getId());
        System.out.println("Descripcion del producto a actualizar " + product.getDesc());
        System.out.println("Precio del producto a actualizar " + product.getPrice());

        Optional<ProductModel> productUpdate = productService.findById(id);
        if(productUpdate.isPresent()){
            System.out.println("Producto para actualizar encontrado. Se actualiza");
            productService.update(product);
        }

        return new ResponseEntity<>(
                product,
                productUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");
        System.out.println("Id del producto a eliminar " + id);
        boolean deleteProduct = productService.delete(id);
        return new ResponseEntity<>(
                deleteProduct ? "Producto borrado" : "Producto no borrado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
