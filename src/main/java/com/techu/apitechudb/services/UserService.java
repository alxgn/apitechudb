package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll(){
        System.out.println("findAll en UserService");

        return this.userRepository.findAll();
    }

    public Optional<UserModel> findById(String id){
        System.out.println("findById en UserService");

        return this.userRepository.findById(id);
    }

    public List<UserModel> findAllOrderBy(String orderBy){
        System.out.println("findAllOrderBy en UserService");

        return this.userRepository.findAll(Sort.by(Sort.Direction.ASC, orderBy));
    }

    public UserModel add(UserModel product){
        System.out.println("add en UserService");

        return this.userRepository.save(product);
    }

    public UserModel update(UserModel user){
        System.out.println("update en UserService");

        return this.userRepository.save(user);
    }

    public boolean delete(String id){
        System.out.println("delete en UserService");
        boolean result = false;
        Optional<UserModel> usuarioBorrar = findById(id);
        if(usuarioBorrar.isPresent()) {
            System.out.println("El registro indicado existe. Se va a borrar");
            result = true;
            userRepository.deleteById(id);
        }

        return result;
    }
}
